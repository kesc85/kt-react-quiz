'use strict';

console.log('Loading function ktReactQuiz-getQuestions');

const quizDbTools = require('./quiz-tools/quiz-db-tools.js');

exports.handler = (payload, context) => {

  const quizName = payload.quizName;

  quizDbTools.queryQuestions(quizName, (quizData) => {
    quizData.Items.forEach((value, index) => {
      delete quizData.Items[index].correctAnswerIdx;
    });
    console.log("Success");
    context.succeed(quizData.Items);
  });

};