'use strict';

console.log('Loading function ktReactQuiz-submitAnswers');

const quizDbTools = require('./quiz-tools/quiz-db-tools.js');
const quizProcessing = require('./quiz-tools/quiz-processing.js');


exports.handler = (payload, context) => {

  const quizName = payload.quizName;
  const userName = payload.user.name;

  quizDbTools.queryQuestions(quizName, (queryData) => {
    quizProcessing.checkAnswers(payload, queryData, (recordObject, returnObject) => {
      quizDbTools.saveResultsToDatabase(recordObject, (recordObject) => {
        console.log("recording results SUCCESS! userName: ", userName);
        context.succeed(returnObject)
      });
    });
  });

};