'use strict';

const doc = require('dynamodb-doc');

const dynamo = new doc.DynamoDB();


exports.saveResultsToDatabase = (recordObject, callback) => {

  const tableName = 'quiz_answers';
  const params = {
    TableName: tableName,
    Item: recordObject
  };

  dynamo.putItem(params, (err, data) => {
    if (err) {
      console.error("Unable to record results. Error: ", JSON.stringify(err, null, 2));
    } else {
      return callback();
    }
  });

};


exports.queryQuestions = (quizName, callback) => {

  const questionFrom = 1;
  const questionTo = 10;
  const params = {
    TableName: "quiz_questions",
    KeyConditionExpression: "#quiz = :quiz and #questionNr between :value1 and :value2",
    ExpressionAttributeNames: {
      "#quiz": "quizName",
      "#questionNr": "questionNr"
    },
    ExpressionAttributeValues: {
      ":quiz": quizName,
      ":value1": questionFrom,
      ":value2": questionTo
    }
  };

  dynamo.query(params, (err, data) => {
    if (err) {
      console.error("Unable to query questions. Error:", JSON.stringify(err, null, 2));
    } else {
      return callback(data);
    }
  });

};