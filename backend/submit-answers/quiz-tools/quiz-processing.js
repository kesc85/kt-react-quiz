'use strict';

exports.checkAnswers = (payload, queryData, callback) => {

  const quizName = payload.quizName;
  const answers = payload.answers;
  const startTimestamp = payload.startTimestamp;
  const endTimestamp = payload.endTimestamp;
  const userName = payload.user.name;
  const timeToComplete = Math.round((endTimestamp - startTimestamp) / 1000);

  let wrongAnswersData = [];
  let correctAnswersCount = 0;
  const questionsCount = queryData.Items.length;

  queryData.Items.forEach((value, index) => {
    if (value.correctAnswerIdx === answers[index]) {
      correctAnswersCount++;
    } else {
      wrongAnswersData.push({
        question: value.question,
        userAnswer: value.possibleAnswers[answers[index]],
        rightAnswer: value.possibleAnswers[value.correctAnswerIdx]
      });
    }
  });

  const scorePct = Math.round(correctAnswersCount / questionsCount * 100);
  const unansweredQuestions = questionsCount - answers.length;
  if (unansweredQuestions !== 0) {
    console.log("NOT ALL ANSWERS RECEIVED! Number of unanswered questions: ", unansweredQuestions);
  }

  let wrongQuestionsMsg;
  if (scorePct === 100) {
    wrongQuestionsMsg = 'You have answered all questions correct!';
  } else {
    wrongQuestionsMsg = 'You have answered these questions WRONG:';
  }

  const returnObject = {
    scorePct,
    wrongAnswersData,
    timeToComplete,
    wrongQuestionsMsg
  };

  const recordObject = {
    userName,
    timestamp: endTimestamp,
    scorePct,
    timeToComplete,
    wrongAnswersData,
    quizName,
    answers
  };

  return callback(recordObject, returnObject);

};
