# README #

**Quiz**

- Version: 1.0.0

### Description ###

- Person must must enter her/his name to start the quiz
- When all questions are answered, result and time is shown
- If not all answers are correct, then questions with wrong answers are shown, including user answer and correct answer
- Data is saved to Database
- Questions are taken from database

### Tech:  ###

- Backend: Node.js (AWS Lambda)
- Frontend: React


### How to deploy? ###

**Backend:**

1. Create DynamoDB table:
   Table name: quiz_questions,
   Primary partition key: quizName (String),
   Primary sort key: questionNr (Number).

2. Import "backend/data/quiz_questions_data.csv" file to DynamoDB table quiz_questions.

3. Create DynamoDB table:
   Table name: quiz_answers,
   Primary partition key: userName (String),
   Primary sort key: timestamp (Number).

4. Create AWS Lambda function "ktReactQuiz-getQuestions" from file "backend/get-questions/get-questions.zip"

5. Create AWS Lambda function "ktReactQuiz-submitAnswers" from file "backend/submit-answers/submit-answers.zip"


**Frontend:**

Copy contents of dist folder to your webserver folder.



### Who do I talk to? ###

* Kestutis Tirksliunas
* k.tirksliunas@gmail.com