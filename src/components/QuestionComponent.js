'use strict';

import React, { Component } from 'react';
import Answer from '../components/AnswerComponent'

require('styles/Question.css');

const QuestionComponent = (props) => {

  const possibleAnswers = props.questionData.possibleAnswers.map((possible_answer) => {
    const answerIdx = props.questionData.possibleAnswers.indexOf(possible_answer);
    return <Answer handleAnswerSelect={props.handleAnswerSelect}
                   key={answerIdx}
                   answerIdx={answerIdx}
                   currentQuestionIdx={props.currentQuestionIdx}
                   answer={possible_answer}/>

  });

  return (
    <div className="question-component">
      {props.questionData.question}
      <ul>
        {possibleAnswers}
      </ul>
    </div>
  );

};

QuestionComponent.displayName = 'QuestionComponent';

// Uncomment properties you need
// QuestionComponent.propTypes = {};
// QuestionComponent.defaultProps = {};

export default QuestionComponent;
