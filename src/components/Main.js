require('normalize.css/normalize.css');
require('styles/App.css');

import React, { Component } from 'react';
import Question from '../components/QuestionComponent';
import Results from '../components/ResultsComponent';
import FirstScreen from '../components/FirstScreenComponent';

const quizName = 'kt-react-quiz';
const awsIdentityPoolId = 'us-east-1:dfdbe772-429a-4b40-8b19-da345cd2806b';
const awsLambdaRegion = 'us-east-1';
const awsLambdaEndpoint = 'lambda.us-east-1.amazonaws.com';
const awsFuncGetQuestions = 'ktReactQuiz-getQuestions';
const awsFuncSubmitAnswers = 'ktReactQuiz-submitAnswers';
const noNameMessage = 'Enter your name before starting!';
const reactLogoImage = require('../images/react_logo.png');

class AppComponent extends Component {

  constructor(props) {

    super(props);

    this.state = {
      questions: [{question: 'Waiting for the data...', possibleAnswers: ['Loading...']}],
      currentQuestionIdx: 0,
      startQuiz: false,
      noNameMsg: '',
      user: {name: ''},
      answers: [],
      results: {result: 'no result yet'}
    };

    AWS.config.region = awsLambdaRegion;
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: awsIdentityPoolId
    });

    const lambda = new AWS.Lambda({
      region: awsLambdaRegion,
      endpoint: awsLambdaEndpoint
    });
    const params = {
      FunctionName: awsFuncGetQuestions,
      Payload: JSON.stringify({quizName})
    };
    lambda.invoke(params, function (err, data) {
      const questions = JSON.parse(data.Payload);
      this.setState({
        questions,
        questionsLeft: questions.length - this.state.currentQuestionIdx
      });
    }.bind(this));

  }

  submitAnswers() {

    const lambda = new AWS.Lambda({
      region: awsLambdaRegion,
      endpoint: awsLambdaEndpoint
    });
    const currentDate = new Date();
    const endTimestamp = currentDate.getTime();
    const params = {
      FunctionName: awsFuncSubmitAnswers,
      Payload: JSON.stringify({
        quizName,
        user: this.state.user,
        answers: this.state.answers,
        startTimestamp: this.state.startTimestamp,
        endTimestamp
      })
    };
    lambda.invoke(params, function (err, data) {
      let results = JSON.parse(data.Payload);
      this.setState({
        results
      });
    }.bind(this));

  }

  handleAnswerSelect(currentQuestionIdx, userAnswerIdx) {

    currentQuestionIdx++;
    this.setState({
      currentQuestionIdx: currentQuestionIdx++,
      answers: [...this.state.answers, userAnswerIdx],
      questionsLeft: this.state.questions.length - currentQuestionIdx + 1
    }, () => {
      if (this.state.questions.length - currentQuestionIdx + 1 === 0) {
        this.submitAnswers();
      }
    });

  }

  setUserName(name) {

    let noNameMsg;
    if (name !== '') {
      noNameMsg = '';
    } else {
      noNameMsg = noNameMessage;
    }
    this.setState({
      user: {name},
      noNameMsg
    });

  }

  startQuiz() {

    const currentDate = new Date();
    const currentTimeStamp = currentDate.getTime();

    if (this.state.user.name == '') {
      this.setState({
        noNameMsg: noNameMessage
      });
    } else {
      this.setState({
        noNameMsg: '',
        startQuiz: true,
        startTimestamp: currentTimeStamp
      });
    }

  }

  customRender() {

    if (!this.state.startQuiz) {

      return (
        <div>
          <FirstScreen
            setUserName={this.setUserName.bind(this)}
            userName={this.state.user.name}
            startQuiz={this.startQuiz.bind(this)}
            noNameMsg={this.state.noNameMsg}/>
        </div>
      );

    } else {

      if (this.state.questionsLeft !== 0) {
        return (
          <div>
            {this.renderQuiz()}
          </div>
        );
      } else {
        return (
          <div>
            <Results resultsData={this.state.results}/>
          </div>
        );
      }

    }

  }


  renderQuiz() {

    return (
      <div>
        Questions left: {this.state.questionsLeft}<br /><br />
        <Question
          handleAnswerSelect={this.handleAnswerSelect.bind(this)}
          currentQuestionIdx={this.state.currentQuestionIdx}
          questionData={this.state.questions[this.state.currentQuestionIdx]}/>
      </div>
    );

  }


  render() {

    return (
      <div className="index">
        <img src={reactLogoImage} alt="React JS"/>
        <div className="notice">
          {this.customRender()}
        </div>
      </div>
    );

  }

}

AppComponent.defaultProps = {};

export default AppComponent;
