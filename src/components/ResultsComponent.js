'use strict';

import React from 'react';
import WrongAnswer from '../components/WrongAnswerComponent'

require('styles/Results.css');

const ResultsComponent = (props) => {

  if (!props.resultsData.wrongAnswersData) {
    return <div>Loading results...</div>
  }

  const wrongAnswers = props.resultsData.wrongAnswersData.map((wrongAnswer) => {
    const answerIdx = props.resultsData.wrongAnswersData.indexOf(wrongAnswer);
    return <WrongAnswer key={answerIdx} wrongAnswer={wrongAnswer}/>
  });

  return (
    <div className="results-component">
      <div className="results-thankyou-text">
        Thank you for taking this quiz! Your score was recorded to our database.
      </div>
      <div className="results-score">
        Your score: {props.resultsData.scorePct}% <br />
        Time: {props.resultsData.timeToComplete} seconds
      </div>

      <div className="results-explanation">
        {props.resultsData.wrongQuestionsMsg}
      </div>

      <ul>
        {wrongAnswers}
      </ul>

    </div>
  );

};

ResultsComponent.displayName = 'ResultsComponent';

// Uncomment properties you need
// ResultsComponent.propTypes = {};
// ResultsComponent.defaultProps = {};

export default ResultsComponent;
