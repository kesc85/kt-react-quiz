'use strict';

import React from 'react';

require('styles/WrongAnswer.css');

let WrongAnswerComponent = (props) => {

  return (
    <div className="wronganswer-component">
      <div className="wronganswer-question">
        {props.wrongAnswer.question}
      </div>
      <div className="wronganswer-wrong">
        Your answer: {props.wrongAnswer.userAnswer}
      </div>
      <div className="wronganswer-right">
        Right answer: {props.wrongAnswer.rightAnswer}
      </div>
    </div>
  );

};

WrongAnswerComponent.displayName = 'WrongAnswerComponent';

// Uncomment properties you need
// WrongAnswerComponent.propTypes = {};
// WrongAnswerComponent.defaultProps = {};

export default WrongAnswerComponent;
