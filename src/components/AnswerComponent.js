'use strict';

import React from 'react';

require('styles/Answer.css');

let AnswerComponent = (props) => {

  return (
    <div className="answer-component">
      <li onClick={() => props.handleAnswerSelect(props.currentQuestionIdx, props.answerIdx)}>{props.answer}</li>
    </div>
  );

};

AnswerComponent.displayName = 'AnswerComponent';

// Uncomment properties you need
// AnswerComponent.propTypes = {};
// AnswerComponent.defaultProps = {};

export default AnswerComponent;
