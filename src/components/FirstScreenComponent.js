'use strict';

import React, { Component } from 'react';

require('styles/FirstScreen.css');

class FirstScreenComponent extends Component {

  constructor(props) {
    super(props);
  }

  handleChange(e) {
    this.props.setUserName(e.target.value);
  }

  startQuiz() {
    this.props.startQuiz();
  }

  render() {

    return (
      <div className="firstscreen-component">
        <div className="welcome-element">
          Welcome to React quiz!
        </div>
        <div className="quiz-form-element">
          Please enter your full name and start the quiz:
        </div>
        <div className="msg-element">
          {this.props.noNameMsg}
        </div>
        <div className="quiz-form-element">
          <input className="input-style" onChange={this.handleChange.bind(this)} placeholder="Name"/>
        </div>
        <div className="quiz-form-element">
          <button className="btn-style" onClick={this.startQuiz.bind(this)}>Start the quiz!</button>
        </div>
      </div>
    );

  }

}

FirstScreenComponent.displayName = 'FirstScreenComponent';

// Uncomment properties you need
// FirstScreenComponent.propTypes = {};
// FirstScreenComponent.defaultProps = {};

export default FirstScreenComponent;
